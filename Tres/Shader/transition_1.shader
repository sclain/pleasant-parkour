shader_type canvas_item;
render_mode unshaded;

uniform float cutoff : hint_range(0,1.0);
uniform sampler2D filter : albedo_hint;
uniform float border : float;
uniform bool inactive: true;

void fragment(){
	if (inactive){
		COLOR = vec4(0.0, 0.0, 0.0, 0.0);
		return;
	}
	float value = texture(filter, SCREEN_UV).r;
  	if (value < cutoff) {
   		COLOR = vec4(0.0, 0.0, 0.0, 1.0);
 	 } 
	else if (value < cutoff+border){
		COLOR = vec4(0.0, 0.0, 0.0, (cutoff + border - value) / border);
	}
	else {
   		COLOR = vec4(0.0, 0.0, 0.0, 0.0);
 	}

}