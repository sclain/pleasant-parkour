shader_type canvas_item;
uniform vec4 color : hint_color;
uniform bool grayscale_active : bool = false;
uniform float amount = 1.0;

void fragment() {
	if (grayscale_active){
		float cp = (texture(TEXTURE, UV).r + texture(TEXTURE, UV).g +texture(TEXTURE, UV).b)/3.0;
		COLOR = texture(TEXTURE, UV) * vec4(amount) + (vec4(1) - vec4(amount)) * vec4(cp);
	}
	else{
		COLOR = texture(TEXTURE, UV);
	}
}

vec4 funi(int hi){
	return vec4(1.0,1.0,1.0,1.0);
}

