extends Node

export (PackedScene) var Grappler
export (PackedScene) var Impulse

signal shoot
signal switch_item
onready var parent = get_parent()
onready var inv = get_parent().get_node("Inventory")
var grapM
var impulse_greM

const ITEM_MANAGER = preload("Items/ItemManager.gd") # Relative path


# var grap
# var can_grap = true
# var imp_gre
# var can_impulse_gre = true

var can_shoot_joy_axis = true

func _ready():
	grapM = ITEM_MANAGER.new()
	impulse_greM = ITEM_MANAGER.new()
	grapM.name = "Grappler"
	grapM.inst = Grappler
	impulse_greM.name = "Impulse"
	impulse_greM.inst = Impulse

func _process(delta):
	if !parent.alive:
		return
		
#	var shoot_joy_axis = Vector2(Input.get_joy_axis(0,JOY_AXIS_2), Input.get_joy_axis(0,JOY_AXIS_3))
	var shoot_mouse = Input.is_action_just_pressed('Shoot')
	var switch_item = Input.is_action_just_pressed('Switch')
	var train_mode = Input.is_action_just_pressed('Training_mode')
	if shoot_mouse:
		var origin = parent.get_global_transform_with_canvas().get_origin()
		var target = get_viewport().get_mouse_position() - origin
		try_shoot(target)
#	if (abs(shoot_joy_axis.x) > 0.7 or abs(shoot_joy_axis.y) >0.7) and can_shoot_joy_axis:
#		can_shoot_joy_axis = false
#		try_shoot(shoot_joy_axis.normalized())
	if switch_item:
		inv.switch_item()
		emit_signal("switch_item")
	if train_mode:
		parent.setTrain_mode()
	
func try_shoot(target):
	if inv.selected==null:
		return
	if inv.items[inv.selected] == "Grappler":
		try_item(grapM, target)
	elif inv.items[inv.selected] == "Impulse_gre":
		try_item(impulse_greM, target)
					
func update_laser(target):
	pass
#	parent.laser.show()
#	if abs(target.x) < abs(joy_axis_temp.x) and abs(target.y) < abs(joy_axis_temp.y):
#		return
#	joy_axis_temp = target
#	target = target.normalized()
#	var rot = atan2(target.y,target.x) - parent.laser.rotation
#	parent.laser.rotation += rot
#	var size = parent.laser.texture.get_size()
#	var new_pos = parent.laser.position - (parent.laser.position-
		

func reset():
	grapM.reset()
	impulse_greM.reset()


func try_item(item, target):
	item.spawn(target, self, parent)
	
			

