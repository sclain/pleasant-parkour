extends KinematicBody2D

signal death
signal floor_zero_enter
signal floor_zero_leave
signal training(val)
signal pick(item)
signal drop_items()

export (int) var MAX_RUN_SPEED_BASE = 400
export (float) var FLOOR_RUN_SPEED= 0.20
export (int) var JUMP_SPEED = -700
export (float) var JUMP_LITTLE = 0.1
export (float) var JUMP_MEDIUM = 0.5
export (float) var CROUCH_SPEED = 0.6
export (int) var GRAVITY = 1200
export (float) var FRICTION_BASE = 0.5
export (float) var AIR_SPEED_MULT= 0.4
export (int) var MAX_FALL_SPEED = 1200
export (int) var GLIDING_SPEED = 50
export (int) var GLIDING_RUN_FORCE = 1.5
export (float) var ICE_DURATION = 7
export (int) var GRAP_SPEED = 850
export (float) var BOUNCE_JUMP = 1.3

const WALL_PENALTY_BASE = 0.5
var own_gravity = 0
var floor_max_angle = deg2rad(55)
var speed = FLOOR_RUN_SPEED
var max_run_speed
var velocity = Vector2()
var last_velocity = Vector2()
var turning = "right"
var running = false
var impulse_force = Vector2(0,0)
var sliding = false
var slide_hit = null
var slide_from_air = false
var new_slide = false
var floor_sliding = false
var crouching = false
var start_jumping = null
var wall_sliding = false
var wall_sliding_origin = null
var inair = false
var iced = false
var gliding = false
var grapping = false
var grap_direction = Vector2()
var grap_target = Vector2(0,0)
var grap_start_position =  Vector2(0,0)
var grap_wall_slided = false
var run_force = 1
var wall_penalty = 0.5
var wall_penalty_down = 0.96
var training = false
var custom_spawn = false
var custom_spawn_position = Vector2()
var boost_type
var is_spawnkill = false

var friction = FRICTION_BASE

var alive = false
var haveKey = false
var invincible = false

var runside
var timers = {}
var timer
var in_game
var width = 32
var height = 80
var crouch_height = 56

#cache the sprite here for fast access (we will set scale to flip it often)
onready var sprite = $Sprite
onready var top_body = $Top_body
onready var camera = $Camera2D
onready var laser = $Laser
onready var inventory = $Inventory
onready var action = $PlayerAction

func _ready():
	G.player = self
	floor_max_angle = deg2rad(55)
	speed = FLOOR_RUN_SPEED
	max_run_speed =  MAX_RUN_SPEED_BASE
	run_force = 1
	friction = FRICTION_BASE
	createTimers()
	var root_name = get_parent().get_name()
	if root_name=="Main_Game":
		in_game = true
		

func createItems(items):
	$Inventory.create(items)

	
func createTimers():
	timers["stop_ice"] =  G.create_timer(ICE_DURATION, self,"stop_ice")
	timers["respawn"] =  G.create_timer(1, self, "respawn")
	timers["camera_teleport"] =  G.create_timer(1, self, "after_respawn")
	timers["wall_slide"] =  G.create_timer(0.5, self, "stop_wall_slide")
	timers["floor_slide"] =  G.create_timer(0.2, self, "stop_floor_slide")
	timers["spawnkill"] = G.create_timer(1.3, self, "_on_stop_spawnkill")
	
func get_input():
	running = false
	var right = Input.is_action_pressed('Move_right')
	var left = Input.is_action_pressed('Move_left')
	var jump = Input.is_action_just_pressed('Jump')
	var crouch = Input.is_action_just_pressed('Crouch')
	var suicide = Input.is_action_just_pressed('Suicide')
	if crouch:
		try_crouch()
	if is_on_floor():
		if inair:
			run_force *= 1/AIR_SPEED_MULT
			inair = false
			enter_floor_zero()
		if jump and try_stop_crouch():
			jump()
	else:
		if !inair:
			inair = true
			run_force *= AIR_SPEED_MULT
			leave_floor_zero()
		try_stop_crouch()
		if wall_sliding:
			if jump:
				jump()
				stop_wall_slide()
	if grapping:
		if jump:
			jump()
			stop_grap()
			wall_slide(0.95, "Grappler")
		else:
			grap()
	elif right:
		runside = "right"
		run(run_force)
	elif left:
		runside = "left"
		run(-run_force)
	else:
		velocity.x = lerp(velocity.x, 0, friction)
	state(runside)
	if suicide:
		emit_signal("death")
	if sliding:
		if is_on_floor() and slide_hit==null and new_slide:
			floor_slide()
	
func _physics_process(delta):
	if !alive:
		sprite.play("idle")
		return		
	get_input()
	last_velocity = velocity
	if !grapping:
		velocity.y += (GRAVITY + own_gravity)* delta
	apply_wall_penalty()
	velocity = move_and_slide(velocity, Vector2(0, -1), 5, 4, 0.7)

func enter_floor_zero():
	if position.y>-150:
		emit_signal("floor_zero_enter")
		
func leave_floor_zero():
	emit_signal("floor_zero_leave")
		
func apply_wall_penalty():
	if is_on_wall() and !sliding:
		if velocity.y<0:
			velocity.y*= wall_penalty
		else:
			velocity.y*= wall_penalty_down
		
func jump():
	var speed = JUMP_SPEED
	if is_on_floor():
		if wall_sliding and wall_sliding_origin=="Grappler":
			if abs(grap_start_position.y - grap_target.y)>150:
				if timers["wall_slide"].wait_time -timers["wall_slide"].time_left<0.12:
					speed*=BOUNCE_JUMP
		elif floor_sliding and slide_from_air and impulse_force.y>0:
				if abs(timers["floor_slide"].wait_time-timers["floor_slide"].time_left)<0.12:
					speed*=BOUNCE_JUMP
	if velocity.y>speed:
		velocity.y = speed
		start_jumping = 0

	
func run(force):
	running = true
	if !sliding:
		velocity.x = lerp(velocity.x, sign(force)*max_run_speed, abs(force)*speed)
	if sliding:
		slide(force)
		if !iced:
			return
	run_clamp()

func run_clamp():
	velocity.x = round(clamp(velocity.x, -max_run_speed, max_run_speed))
	
func slide(force):
	if iced:
		velocity.x += MAX_RUN_SPEED_BASE*speed*force
	else:
		var fric = FRICTION_BASE/10
		var modifier = 	pow(2, -(sign(force)*sign(velocity.x)))
		fric*= modifier
		velocity.x = lerp(velocity.x, 0, fric)
	if abs(velocity.x) <= MAX_RUN_SPEED_BASE and !iced:
		stop_setBoost()
	
func state(runside):
	if grapping:
		sprite.play("idle")
	elif crouching:
		sprite.play("crouch")
	elif running and is_on_floor() and !iced:
		sprite.play("run")
	elif inair:
		if position.y > 0:
			emit_signal("death")
		elif velocity.y<0:
			anim_jump()
		elif velocity.y > MAX_FALL_SPEED or gliding:
			glide()
		else:
			anim_fall()
	else:
		sprite.play("idle")
		
	if runside == "right" and turning == "left":
		flip()
		turning = "right"
	elif runside =="left" and turning == "right":
		flip()
		turning = "left"
	if start_jumping!=null:
		cut_jump()
	if gliding:
		sprite.play("glide")
		if is_on_floor() or is_on_wall():
			stop_glide()
	if is_on_ceiling():
		velocity.x*=1.5

func anim_jump():
	sprite.play("jump")

func anim_fall():
	sprite.play("fall")
	
func glide():
#	if iced:
#		stop_ice()
	if grapping:
		stop_glide()
	gliding = true
	anim_fall()
	velocity.y = lerp(velocity.y, GLIDING_SPEED, 0.1)
	run_force  =  GLIDING_RUN_FORCE
	
func stop_glide():
	gliding = false
	
	
func cut_jump():
	if Input.is_action_just_released("Jump") or start_jumping>20:
		if start_jumping<7:
			velocity.y*=JUMP_LITTLE
		elif start_jumping<14:
			velocity.y*=JUMP_MEDIUM
		start_jumping = null
	else:
		start_jumping+=1
				
func flip():
	sprite.scale.x *= -1
		
func impulse(force, fric=0.05, b_t=1):
	print("impulse:, ", alive)
	print(position)
	impulse_force = force
	force.x *=1.5
#	setBoost(0.05, abs(force.x))
	setBoost(fric, MAX_RUN_SPEED_BASE, b_t)
#	velocity.x = sign(force.x)*abs(velocity.x)+force.x
#	velocity.y = sign(force.y)*abs(velocity.y)+force.y
	velocity = force
	
func bounce():
	velocity.x = last_velocity.x*-1
	velocity.y = last_velocity.y*-1
	
func ice():
	var time = timers["stop_ice"]
	if iced:
		color(Color(0.5,0.5,1,1))
		time.wait_time = time.time_left + ICE_DURATION
		time.wait_time = clamp(time.wait_time, 0, ICE_DURATION*1.5)
		time.start()
	else:
		time.wait_time = ICE_DURATION
		time.start()
		color(Color(0.5,0.7,0.9,1))
		setBoost(0.01, MAX_RUN_SPEED_BASE*2)
		speed = 0.05
		iced = true

	
func stop_ice():
	if iced:
		timers["stop_ice"].stop()
		color(ColorN("white",1))
		iced = false
		setBoost(0.1)
	
func setBoost(fric=FRICTION_BASE, max_rs=MAX_RUN_SPEED_BASE, b_t=1):
	slide_from_air = inair
	new_slide = true
	if gliding:
		stop_glide()
	if iced:
		return
	friction = fric
	max_run_speed = max_rs
	if grapping:
		stop_grap()
	if crouching:
		max_run_speed*=CROUCH_SPEED
	sliding = true
	boost_type = b_t
		
func stop_setBoost():
	friction = FRICTION_BASE
	speed = FLOOR_RUN_SPEED
	max_run_speed = MAX_RUN_SPEED_BASE
	if crouching:
		max_run_speed*=CROUCH_SPEED
	sliding = false
#	if boost_type==3:
#		wall_slide(0.95, "Impulse_gre")
	slide_hit = null
	

func start_grap(target):
	if gliding:
		stop_glide()
	grap_target = target
	grap_direction = (target - position).normalized()
	grapping = true
	grap_start_position = position
	velocity = grap_direction*GRAP_SPEED
	grap_wall_slided = false
	if sliding:
		stop_setBoost()
	
func grap():
	if abs(grap_target.x) > abs(grap_start_position.x):
		if abs(position.x)>abs(grap_target.x):
			stop_grap()
			return
	else:
		if abs(position.x)<abs(grap_target.x):
			stop_grap()
			return
	if abs(velocity.x) < 500 and abs(velocity.y)<500:
		stop_grap()
		return
	velocity = grap_direction*GRAP_SPEED
	if !wall_sliding and !grap_wall_slided:
		var colli_ws = test_move(transform, Vector2(sign(velocity.x)*50,sign(velocity.y)*50))
		if colli_ws:
			grap_wall_slided = true
			wall_slide(0.95, "Grappler")
	if is_on_wall():
		velocity*=0.5
	
func stop_grap():
	grapping = false

func wall_slide(val, origin):
	wall_sliding_origin = origin
	wall_sliding = true
	timers["wall_slide"].start()
	wall_penalty = val
	
func stop_wall_slide():
	wall_penalty = WALL_PENALTY_BASE
	wall_sliding = false

func floor_slide():
	new_slide = false
	floor_sliding = true
	slide_hit = "floor"
	timers["floor_slide"].start()
	
func stop_floor_slide():
	slide_hit = null
	floor_sliding = false
	pass
	
func try_crouch():
	if !top_body.disabled and !grapping:
		if is_on_floor():
			crouch()
	else:
		try_stop_crouch()
				
func crouch():
	max_run_speed*=CROUCH_SPEED
	crouching = true
	top_body.disabled = true

func try_stop_crouch():
	if crouching:
		top_body.disabled = false
		if !test_move(transform, Vector2(0,-5)):
			max_run_speed *= 1/CROUCH_SPEED
			crouching = false
			return true
		else:
			top_body.disabled = true
			return false
	return true

func add_balloon(val : int):
	print("addbaloon")
	own_gravity += -700*val

func die():
	alive = false
	$PlayerAction.reset()
	stop_state()
	timers["respawn"].start()
	color(ColorN("red",1))

func reset(training_die= true):
	$PlayerAction.reset()
	if training:
		setTrain_mode(training_die)
			
func respawn():
	camera.smoothing_enabled = false
	color(ColorN("white",1))
	if in_game:
		if custom_spawn:
			position = custom_spawn_position
		else:
			position = get_parent().get_node("Level").get_node("RESPAWN").position
		timers["camera_teleport"].start()
		haveKey = false
	if training == true:
		if is_spawnkill:
			setTrain_mode()
	is_spawnkill = true
	timers["spawnkill"].start()
	alive = true

func stop_state():
	stop_ice()
	stop_glide()
	stop_setBoost()
	stop_grap()
	try_stop_crouch()
	velocity = Vector2(0,0)

func _haveKey():
	if haveKey:
		return true
	else:
		if !training:
			haveKey = true
			return false
	return true
		
func setTrain_mode(die = true):
	if alive:
		if training:
			emit_signal("training", false)
			if die:
				die()
			training = false
			custom_spawn = false
		else:
			if !haveKey and !crouching:
				emit_signal("training", true)
				custom_spawn = true
				custom_spawn_position = position
				training = true
	
func after_respawn():
	camera.teleport()
	invincible = false

func color(color):
	sprite.modulate = color

func _on_stop_spawnkill():
	is_spawnkill = false

	






