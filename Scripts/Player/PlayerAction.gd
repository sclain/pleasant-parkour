extends Node

signal shoot
signal switch_item
signal reload_items(array)

onready var parent = get_parent()
onready var inv = get_parent().get_node("Inventory")

var itemScene = []
var itemManager = {}

const ITEM_MANAGER = preload("Items/ItemManager.gd") # Relative path


func _ready():
	for i in range(G.projectile.size()):
		var item = G.projectile[i]
		itemManager[item] = ITEM_MANAGER.new()
		itemManager[item].name = item
		itemManager[item].inst = load(str("res://Nodes/Items/",item,".tscn"))


func _process(delta):
	if !parent.alive:
		return
	var shoot_mouse = Input.is_action_just_pressed('Shoot')
	var switch_item = Input.is_action_just_pressed('Switch_weapon')
	var train_mode = Input.is_action_just_pressed('Training_mode')
	if shoot_mouse:
		var origin = parent.get_global_transform_with_canvas().get_origin()
		var target = get_viewport().get_mouse_position() - origin
		try_shoot(target)
	if switch_item:
		inv.switch_item()
		emit_signal("switch_item")
	if train_mode:
		parent.setTrain_mode()
	
func try_shoot(target):
	if inv.selected==null:
		return
	try_item(itemManager[inv.items[inv.selected]], target)
					

func reset():
	for i in itemManager:
		itemManager[i].reset()	
	floor_reload_all_items()

func try_item(item, target):
	if item.spawn(target, self, parent):
		send_floor_reload_items()

func _on_Player_floor_zero_enter():
		floor_reload_all_items()	

func reload_one_item(id):
	itemManager[id].loaded = true
	send_floor_reload_items()
	
func _on_Player_floor_zero_leave():
	if get_parent().position.y>-150:
		floor_reload_all_items()
			
func send_floor_reload_items():
	var array = []
	for i in inv.items:
		array.append(itemManager[i].loaded)
	emit_signal("reload_items", array)
	
func floor_reload_all_items():
	for i in itemManager:
		itemManager[i].loaded = true
	send_floor_reload_items()
		
