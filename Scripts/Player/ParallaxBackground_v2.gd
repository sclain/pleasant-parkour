extends Node
signal set_shader(effect, value)
export (ShaderMaterial) var shader
var shader_amount : float = 1
var duration : float = 1

func _ready():
	for child in $Layers.get_children():
		child.set("material", shader)

func _on_ParallaxBackground_set_shader(effect, value):
	if effect == "grayscale":
		if value:
			$Tween.interpolate_property(self, "shader_amount", 1, 0.2, duration, Tween.TRANS_CIRC, Tween.EASE_OUT)
		else:
			$Tween.interpolate_property(self, "shader_amount", 0.2, 1, duration, Tween.TRANS_CIRC, Tween.EASE_OUT)
		$Tween.start()
		if value:
			for child in $Layers.get_children():
				child.get_material().set_shader_param("grayscale_active", true)
		else:
			G.create_timer(duration, self, "disable_grayscale", true)

func _on_Player_training(val):
	if val:
		emit_signal("set_shader", "grayscale", true)
	else:
		emit_signal("set_shader", "grayscale", false)

func _process(delta):
	for child in $Layers.get_children():
		child.get_material().set_shader_param("amount", shader_amount)
		
		
func disable_grayscale():
	for child in $Layers.get_children():
		child.get_material().set_shader_param("grayscale_active", false)
	