extends Node

var can = true 
var exist = false
var node
var inst
var floor_load = false
var loaded = true


func reset():
	var wr
	if exist:
		wr = weakref(node);
		if wr.get_ref():
			if wr.get_ref().get_name() == name:
				wr.get_ref().stop()
		exist = false
		can = true

func _on_finished():
	can = true
	exist = false

func spawn(target, parent, player):
	var player_on_floor_zero = player.position.y<-150
	if floor_load:
		if !loaded and player_on_floor_zero:
			return false
	if can:
		can = false
		exist = true
		node = inst.instance()
		if ("floor_load" in node):
			if node.floor_load:
				floor_load = true
				if player_on_floor_zero:
					loaded = false
		parent.add_child(node)
		node.connect("finished", self, "_on_finished")
		node.activate(target, player)
		return true
	return false
