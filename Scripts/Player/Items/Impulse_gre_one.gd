extends RigidBody2D
export (int) var power = 1500
export (float) var delay = 1
export (int) var b_t = 3

signal finished
var actived = false
var velocity =  Vector2(0,0)
var speed = 3
var timer

func _ready():
	timer = $Timer.create(delay,self, "stop")

			
func activate(target, parent):
	position = parent.position.round()
	var target_norm = target.normalized()
	position.x+= target_norm.x*25
	position.y+= target_norm.y*25
	var velocity = target
	apply_impulse(Vector2(0,0), velocity*speed)
	timer.start()
	
func stop():
	var collision = $Area.get_overlapping_bodies()
	for i in range(collision.size()):
		if collision[i].is_in_group("player"):
			if collision[i].alive:
				action(collision[i])
	actived = false
	emit_signal("finished")
	queue_free()

func action(target):
	var force = (target.position-position).normalized()
	var distance = (pow((position.x-target.position.x),2))-(pow((position.y-target.position.y),2))
	distance = abs(distance)
	if distance <10:
		distance = distance*10
	distance = sqrt(distance)
	var boost = 1.125
#	if distance<50:
#		boost=1.5
#	elif distance <150:
#		boost=1.25
	target.impulse(force*power*boost, 0.05, b_t)




