extends Area2D

signal pick(item)
export (String) var id = "Grappler_epic"
var picked = false

func _on_Grappler_pick_body_entered(body):
	if !picked:
		if body.is_in_group("player"):
			for i in range(body.inventory.items.size()):
				if body.inventory.items[i] == id:
					body.action.reload_one_item(id)
					return
			body.emit_signal("pick", id)
			picked = true
			hide()

func reset():
	show()
	picked = false