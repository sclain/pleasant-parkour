extends KinematicBody2D

signal finished
var type = "throwable"
var actived = false
var velocity =  Vector2(0,0)
var speed = 1000
var start_position
var _parent
onready var limit = Vector2(640*1.5,480*1.5)

func _ready():
	pass

func _physics_process(delta):
	if actived:
		move()

func move():
	move_and_slide(velocity*speed)
	if get_slide_count()>0:
		_parent.start_grap(position)
		stop()
	elif abs(position.x-start_position.x) > limit.x/2 or abs(position.y-start_position.y) > limit.y/2:
		stop()
	
func activate(target, parent):
	actived = true
	position = parent.position.round()
	start_position = position
	velocity = target.normalized()
	var v2 = Vector2(1,0)
	var rot = atan2(v2.y,v2.x) - atan2(target.y,target.x)
	$Sprite.rotate(-rot)
	_parent = parent
	
func stop():
	emit_signal("finished")
	actived = false
	queue_free()
	

	
	

	
	
	
