extends Sprite
signal finished
var type = "gear"

var actived : bool = false

func activate(target, player):
	player.add_balloon(1)

func stop():
	emit_signal("finished")
	actived = false
	queue_free()