extends Control

var cutoff : float = 0
var duration : float = 0.7
var has_round_trip = false
	
	
func _process(delta):
	pass
#	$ColorRect.get_material().set_shader_param("cutoff", cutoff)
	
func _on_Player_level_change():
	crossfade()
	pass
#	gradient()

#func gradient() -> void:
#	$ColorRect.get_material().set_shader_param("inactive", false)
#	$Tween.interpolate_property(self, "cutoff", 0.0, 1.0, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
#	$Tween.start()
#
#func transition_back() -> void:
#	$Tween.interpolate_property(self, "cutoff", 1.0, 0.0, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
#	$Tween.start()
#
func _on_Tween_tween_completed(object, key):
	hide()
#	if !has_round_trip:
#		transition_back()
#		has_round_trip = true
#	else:
#		has_round_trip = false
#		$ColorRect.get_material().set_shader_param("inactive", true)


func tween_start(parent, prop, from, to):
	$Tween.interpolate_property(self, prop, from, to, duration, Tween.TRANS_CIRC, Tween.EASE_OUT)
	$Tween.start()
	
	
func crossfade():
	show()
	var capture = get_viewport().get_texture().get_data()
	capture.flip_y()
	var texture = ImageTexture.new()
	texture.create_from_image(capture) 
	var img_scale = Vector2(640,480)/OS.window_size
	$TextureRect.set_texture(texture)
	tween_start($TextureRect, "modulate", Color(1,1,1,1), Color(1,1,1,0))	
	pass