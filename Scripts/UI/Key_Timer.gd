extends Label

var color = "normal"

func update_time(time):
	text = str(time)
	

func _on_GUI_timer_zone_change(time, gold):
	if gold and color!="gold":
		add_color_override("font_color", Color(0.65,1,0.35,1))
		color = "gold"
	elif !gold and color!="normal":
		add_color_override("font_color", Color(1,1,1,1))
		color= "normal"
	update_time(round(time))
