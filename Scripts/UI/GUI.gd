extends CanvasLayer

signal g_loaded
signal display_helper_input(action, keys, type)
signal display_helper_msg(msg)
signal level_change(id, gold)
signal timer_zone_change(time)
signal visibility_change(game_paused)
export (bool) var actived = true
onready var ending_screen = $ctrl/Ending_screen
var can_hide = true


func _ready():
	G.pause_node = self
	hide_bg()
	ending_screen.hide()
	if actived:
		$ctrl/Pause.show()
		get_tree().paused = true
	else:
		$ctrl/Pause.hide()
		get_tree().paused = false


func _process(delta):
	var pause = Input.is_action_just_pressed('pause')
	if pause:
		switch_pause()

func switch_pause(hide_bg=false):
	if can_hide:
		if get_tree().paused:
			resume()
		else:
			open()
			if hide_bg:
				hide_bg()

	
func resume():
	$ctrl/Pause.color = Color(0,0,0,0.5)
	$ctrl/Pause.hide()
	actived = false
	get_tree().paused = false
	emit_signal("visibility_change", false)
	
func hide_bg():
	$ctrl/Pause.color = Color(0,0,0,1)
		
func open():
	$ctrl/Pause.show()
	actived = true
	get_tree().paused = true
			
func _on_InputPopup_about_to_show():
	can_hide = false

func _on_InputPopup_popup_hide():
	can_hide = true

func _on_Main_Game_g_loaded():
	emit_signal("g_loaded")

func _on_player_reload_items(array):
	$ctrl/Player.reload_items(array)

func _on_player_switch_item():
	$ctrl/Player.switch_item()

func _on_Main_Game_level_change(id, gold):
	emit_signal("level_change", id, gold)
	pass # replace with function body

func on_timer_zone_change(time, gold):
	emit_signal("timer_zone_change", time, gold)

func _on_Player_helper_msg_hide():
	get_tree().set("paused", false)
	can_hide = true

func _on_Player_helper_msg_show():
	get_tree().set("paused", true)
	can_hide = false
