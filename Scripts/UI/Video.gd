extends PanelContainer

signal set_video_param(param, options)

var video_param  = []
var param

func init():	
	video_param.append({"name": "Fullscreen", "type": "bool", "parent":"OS", "prop":"window_fullscreen", "value": OS.get("window_fullscreen")})
	video_param.append({"name": "Vsync (Recommended)", "type": "bool", "parent":"OS", "prop":"vsync_enabled", "value": OS.get("vsync_enabled")})
	var sz = OS.get_screen_size()
	video_param.append({"name": "Resolution", "type": "list", "parent":"OS", "prop": "window_size", "value": OS.window_size, "option":[ sz,Vector2(sz.y*640/480, sz.y), Vector2(640,480), Vector2(960,720), Vector2(1024,768), Vector2(1280,720), Vector2(1600,900), Vector2(1920,1080)]})
	video_param.append({"name": "Stretch", "type": "bool", "parent":"get_tree()", "prop":"screen_stretch", "value": false})
	for i in range(video_param.size()):
		if G.settings.video.has(video_param[i].name):
			G.set_video_setting(video_param[i], G.settings.video[video_param[i].name]) 
			video_param[i].value = G.settings.video[video_param[i].name]
		emit_signal("set_video_param", video_param[i])


func _on_GUI_g_loaded():
	init()

