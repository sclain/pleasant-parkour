extends HBoxContainer

var current

func _ready():
	for i in range(1, get_child_count()):
		var child = get_child(i)
		if child.is_visible():
			nav(child)

func nav(to):
	if current:
		if current==to:
			if current.is_visible():
				current.hide()
			else:
				current.show()
			return
		current.hide()
	current = to
	current.show()


# buttons:

func _on_Inputs_pressed():
	nav($Inputs)

func _on_Levels_pressed():
	nav($Levels)

func _on_Video_pressed():
	nav($Video)

func _on_GUI_g_loaded():
	$Inputs/ScrollContainer2/MarginContainer/IM_Manager.get_input_data()
	$Levels/ScrollContainer/MarginContainer/Levels_grid.init_button()

func _on_Turn_off_pressed():
	G.leave_game()
	pass # replace with function body
