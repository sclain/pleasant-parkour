extends Control

signal level_change
signal display_helper_input(action, keys, type)
signal display_helper_msg(msg)
signal helper_msg_show
signal helper_msg_hide

export (PackedScene) var node_item
var selected
onready var player_item = $MarginContainer/VBoxContainer/Items
var all_item_actived

func _ready():
	for i in range(G.projectile.size()):
		var n_item = node_item.instance()
		player_item.add_child(n_item)
		var path = str("res://Graphics/Items/",G.projectile[i],".png")
		player_item.items[G.projectile[i]] = n_item
		n_item.texture = load(path)
		
func add_items(array):
	selected = 0
	var child_size = player_item.get_child_count()
	for child in player_item.get_children():
		child.hide()
		player_item.active = []
	for i in range(array.size()):
		var itm = player_item.items[array[i]]
		player_item.active.append(itm)
		itm.modulate = ColorN("white",0.3)
		if player_item.active.size() == 1:
			itm.modulate = ColorN("white",1)
			selected = i
	reload_items(null)
		
func switch_item():
	if player_item.active.size()<2:
		return
	player_item.active[selected].modulate = ColorN("white",0.3)
	if player_item.active.size()>selected+1:
		selected+=1
		player_item.active[selected].modulate = ColorN("white",1)
	else:
		selected = 0
		player_item.active[selected].modulate = ColorN("white",1)
		
func reload_items(array=null):
	if !array:
		for i in range(player_item.active.size()):
			player_item.active[i].show()
	else:
		for i in range(player_item.active.size()):
			if array[i]:
				player_item.active[i].show()
			else:
				player_item.active[i].hide()



func _on_GUI_level_change(id, gold):
	emit_signal("level_change")


func _on_GUI_visibility_change(game_paused):
	if !game_paused:
		$Transition.crossfade()

func _on_GUI_display_helper_input(action, keys, type):
	emit_signal("display_helper_input", action, keys, type)

func _on_GUI_display_helper_msg(msg):
	emit_signal("display_helper_msg", msg)

func _on_Helper_msg_helper_msg_hide():
	emit_signal("helper_msg_hide")

func _on_Helper_msg_helper_msg_show():
	emit_signal("helper_msg_show")

