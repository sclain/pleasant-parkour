extends GridContainer

export (PackedScene) var IM_label
export (PackedScene) var IM_button
export (PackedScene) var Checkbox
export (PackedScene) var OptionBtn


func _on_Video_set_video_param(param, options=null):
	var label = IM_label.instance()
	label.text = param.name
	add_child(label)
	var button
	if param.type=="bool":
		button = Checkbox.instance()	
	elif param.type=="list":
		button = OptionBtn.instance()
	button.init(param)
	button.connect("button_change", self, "_on_button_change")
	add_child(button)


func _on_button_change(param, value, parent=null):
	G.set_video_setting(param, value, parent)
	
	
		
		
	
