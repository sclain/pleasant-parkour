extends CheckBox

signal button_change(param, value)
var param
var value

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func init(prm):
	param = prm
	pressed = prm.value


func _on_CheckBox_video_pressed():
	emit_signal("button_change", param, pressed)

