extends RichTextLabel

func set_text(action, multiple_key=null, type=null):
	if !G.loaded:
		return
	if action =="_reset":
		hide()
		return
	show()
	var txt1 = ""
	var txt2 = ""
	var sep = " and "
	if type=="combo":
		sep = " + "	
	txt1 += G.human_string(action)+": "
	if multiple_key:
		for i in range(multiple_key.size()):
			txt2 += G.settings.input[multiple_key[i]]
			if i<multiple_key.size()-1:
				txt2 +=sep
	else:
		txt2 += G.settings.input[action]
	bbcode_text  = "[center][b]"+txt1+"[/b]"+txt2+"[/center]"

func _on_Player_display_helper_input(action, keys, type):
	set_text(action, keys, type)

