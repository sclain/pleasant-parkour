extends Button

signal set_level(id)
var lvl_id


func setup(txt, id, gold):
	text = txt.replace("_", " ")
	lvl_id = id
	if gold:
		add_color_override("font_color", Color(0.65,1,0.35,1))
	

func _on_Button_pressed():
	emit_signal("set_level", lvl_id)

func set_gold(val):
	if val:
		add_color_override("font_color", Color(0.65,1,0.35,1))

		