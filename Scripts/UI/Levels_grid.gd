extends GridContainer

export (PackedScene) var btn

func add_btn(lvl, id, gold):
	var inst = btn.instance()
	add_child(inst)
	inst.connect("set_level", self, "_on_button_pressed")
	inst.setup(lvl, id, gold)
	if G.level_best<id:
		inst.disabled = true

func init_button():
	for i in range(G.level.size()):
		add_btn(G.level[i].name, i, G.level[i].gold)
	
func _on_button_pressed(id):
	G.set_lvl(id)
	
func set_all_disable():
	for i in range(get_child_count()):
		if G.level_best>=get_child(i).lvl_id:
			get_child(i).disabled = false

func _on_GUI_level_change(id, gold):
	if gold:
		get_child(id).set_gold(true)
	if get_child_count()>id+1:	
		get_child(id+1).disabled = false
