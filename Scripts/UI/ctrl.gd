extends Control

var timer_enable_input

func _ready():
	timer_enable_input = $Timer.create(0.3, self, "_enable_input")
	

func _disable_input():
	get_tree().get_root().set_disable_input(true)
	timer_enable_input.start()
	
func _enable_input():
	get_tree().get_root().set_disable_input(false)

func _on_InputPopup_send_input(input):
	_disable_input()

