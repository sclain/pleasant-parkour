extends OptionButton

signal button_change(param, value)
var param
var value

func init(prm):
	prm.option.sort_custom(G, "sort_Vector2")
	param = prm
	var select
	for i in range(prm.option.size()):
		select = false
		if prm.option[i] == prm.value:
			select = true
		set_item(prm.option[i], i, select)


func set_item(val, id, select = false):
	var txt = String(val).replace("(", "").replace(")", "").replace(",", " *")
	add_item(txt, id)
	set_item_metadata(id, val)
	if select:
		selected = id
		
	
func _on_OptionButton_video_item_selected(ID):
	emit_signal("button_change", param, get_item_metadata(ID))

