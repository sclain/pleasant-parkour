extends WindowDialog

signal send_input(input)
const unwanted = [KEY_ESCAPE, KEY_ENTER]

func _ready():
	rect_size = Vector2(200,60)
	var mx = margin_left
	rect_position.x = rect_position.x-rect_size.x/2-mx*2


func _input(event):
	if is_visible():
		if event is InputEventKey:
			if event is InputEventKey:
				for i in range(unwanted.size()):
					if event.scancode == unwanted[i]:
						return
			send_input(event)
						

func send_input(input):
	emit_signal("send_input", input)
	hide()


func _on_InputPopup_gui_input(event):
	if is_visible():
		if event is InputEventMouseButton:
			send_input(event)


func _on_IM_Manager_popup_input():
	popup()

