extends GridContainer

signal popup_input
signal input_loaded
export (PackedScene) var IM_label
export (PackedScene) var IM_button
var button_active
var timer_input_changed

const INPUT_WANTED = ["Move_left", "Move_right", "Jump", "Crouch", "Shoot", "Switch_weapon", "Suicide", "Training_mode"]
const INPUT_MOUSE = ["Left click", "Right click", "Middle mouse button"]


func get_input_data():
	for i in range(INPUT_WANTED.size()):
		var action = INPUT_WANTED[i]
		if action in G.settings["input"]:
			var input = find_input_from_key(G.settings["input"][action])
			create_button(action, input, G.settings["input"][action])
			var old_input = InputMap.get_action_list(action)[0]
			set_input(action, old_input, input)
		else:
			get_input_from_InputMap(action)
	emit_signal("input_loaded")
			
	
func get_input_from_InputMap(action):
	var input = InputMap.get_action_list(action)[0]
	var key = get_key(input)
	if key:
		create_button(action, input, key)

	
func create_button(action, input, key):
	var label = IM_label.instance()
	add_child(label)
	var action_text = action.replace("_", " ")
	label.text = action_text
	var button = IM_button.instance()
	add_child(button)
	button.text = key
	button.action = action
	button.input = input
	button.connect("send_info", self, "try_input_change")
	G.settings["input"][action] = key
	
func input_wanted(input):
	for i in range(INPUT_WANTED.size()):
		if input == INPUT_WANTED[i]:
			return true
	return false
	
func try_input_change(button):
	emit_signal("popup_input")
	button_active = button
	
func set_input(action, old_input, input, set_button=false):
	var key = get_key(input)
	G.settings["input"][action] = key
	InputMap.action_erase_event(action, old_input)
	InputMap.action_add_event(action, input)
	if set_button:
		button_active.text = key
		button_active.input = input
		button_active.release_focus()
		
func get_key(event):
	var key = false
	if event is InputEventKey:
		key = OS.get_scancode_string(event.scancode)
	elif event is InputEventMouseButton:
		var button = int(event.button_index)-1
		if INPUT_MOUSE.size()> button:
			key = INPUT_MOUSE[button]
		else:
			key = "Unknown button"
	return key
		
func is_mouse_button(key):
	for i in range(INPUT_MOUSE.size()):
		if key == INPUT_MOUSE[i]:
			var input = InputEventMouseButton.new()
			input.button_index = int(INPUT_MOUSE.find(key))+1
			return input
	return false
	
func find_input_from_key(key):
	var is_mouse = is_mouse_button(key)
	var input
	if is_mouse:
		return is_mouse
	input = InputEventKey.new()
	input.scancode = OS.find_scancode_from_string(key)
	return input
	
func _on_InputPopup_send_input(input):
	set_input(button_active.action, button_active.input, input, true)

func _on_Control_pressed():
	pass # replace with function body

func _on_GUI_g_loaded():
	get_input_data()
	pass # replace with function body
