extends MarginContainer

signal helper_msg_show
signal helper_msg_hide
onready var label = $VBoxContainer/Label

func _ready():
	hide()

func _on_Button_helper_msg_pressed():
	hide()
	emit_signal("helper_msg_hide")

func _on_Player_display_helper_msg(msg):
	label.text = msg
	show()
	emit_signal("helper_msg_show")
	pass # Replace with function body.
