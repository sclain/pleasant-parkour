extends Node

signal set_pause
signal set_level(id)
const projectile = ["Grappler", "Grappler_epic", "Impulse_gre", "Impulse_gre_epic"]
var loaded = false
var game_save = false
var level = []
var level_map = {}
var level_node = []
var save = {}
var settings = {}
var time_recorded = 0
var level_id = 0
var level_best = 0
var player
var pause_node
var GUI
var MAIN
var PLAYER

var first="TEST"

var video_stretch = false
			
func _ready():
	settings["input"] = {}
	settings["video"] = {}
	add_level(first)
	add_level("Alberto")
	add_level("Beryl")
	add_level("Chris")
	add_level("Debby")
	add_level("Ernesto")
	add_level("Florence")
	add_level("Gordon")
	add_level("Helene")
	add_level("Isaac")
	add_level("Joyce")
	add_level("Kirk")
	add_level("Leslie")
	add_level("Michael")
	add_level("Nadine")
	add_level("Oscar")
	add_level("Patty")


func add_level(_name):
	var add = {"name":_name, "record": null, "gold": false}
	level_map[_name] = level.size()
	level.append(add)
	level_node.append(load(str("res://Nodes/Main_Scene/Levels/",_name,".tscn")))
	
		
func save_game():
	if !game_save:
		return
	time_recorded = time_recorded + OS.get_ticks_msec()
	var save = {
		"level": level,
		"level_id" : level_id,
		"level_best": level_best,
		"time_recorded": time_recorded,
		}
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://save_game.json", File.WRITE, "gotaga")
	f.store_string(to_json(save))
	f.close()


func save_settings():
	if !game_save:
		return
	var save = {
		"settings": settings
		}
	var save_game = File.new()
	save_game.open("user://save_settings.json", File.WRITE)
	save_game.store_string(to_json(save))
	save_game.close()
		
		
func create_timer(delay, parent, callback, autostart=false):
	var timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time( delay )
	timer.connect("timeout", parent, callback)
	add_child(timer)
	if autostart:
		timer.start()
	return timer
	
func create_loop(delay, parent, callback, autostart=true):
	var timer = Timer.new()
	timer.set_wait_time( delay )
	timer.connect("timeout", parent, callback)
	add_child(timer)
	if autostart:
		timer.start()
	return timer

func set_lvl(id):
	emit_signal("set_level", id)

func set_record(val, id, gold):
	var result = {"record": false, "gold": false}
	if level[id].record:
		if val < level[id].record:
			level[id].record = val
			result["record"] = true
	else:
		level[id].record = val
		result["record"] = true
	if gold and !level[id].gold:
		result["gold"] = true
		level[id].gold  = true
	return result

func sort_Vector2(a, b):
	return a.x < b.x
	
	
func set_video_setting(param, value, parent=null):
	if param["parent"] =="OS":
		if typeof(OS.get(param["prop"])) == TYPE_VECTOR2 and typeof(value) != TYPE_VECTOR2:
			value = string_to_vector2(value)
	elif param["parent"] =="get_tree()":
		if typeof(get_tree().get(param["prop"])) == TYPE_VECTOR2 and typeof(value) != TYPE_VECTOR2:
			value = string_to_vector2(value)
			
	if param["name"] == "Resolution":
		OS.set_window_size(value)
		get_viewport().set_size(value)
		if OS.window_fullscreen:
			OS.set_window_fullscreen(false)
			OS.set_window_fullscreen(true)
	elif param["name"] == "Stretch":
		print(value)
		if value:
			get_tree().set_screen_stretch(1,1,Vector2(640,480))

		else:
			get_tree().set_screen_stretch(1,4,Vector2(640,480))
	elif param["parent"] == "OS":
		OS.set(param["prop"], value)
	settings.video[param["name"]] = value

func string_to_vector2(string):
	string = string.replace("(", "").replace(")", "")
	var arr = string.split(',')
	return Vector2(arr[0],arr[1])
	
func human_string(string):
	return string.replace("_", " ")
	
func collision_body_n_area(target, area):
	var b_x = round(target.position.x-target.width/2)
	var b_y = round(target.position.y-target.height/2)
	var b_width = target.width
	var b_height = target.height
	if target.crouching:
		b_y+=target.height-target.crouch_height
		b_height = target.crouch_height
	var poly = area.get_node("CollisionPolygon2D").polygon
	var p1 = poly[0]
	var p2 = poly[1]
	var p3 = poly[3]
	var rot = round(area.rotation_degrees)	
	var a_width = abs(poly[0].x-poly[1].x)*area.scale.x
	var a_height = abs(poly[0].y-poly[3].y)*area.scale.y
	if rot == 90 or rot == -90:
		var temp = a_width
		a_width = a_height
		a_height = temp
	var a_x = round(area.position.x-a_width/2)
	var a_y = round(area.position.y-a_height/2)
	
	if b_x+b_width < a_x or b_x >a_x+a_width:
		return false
	if b_y+b_height < a_y or b_y >a_y+a_height:
		return false
	return true
	
func leave_game():
	save_game()
	save_settings()
	get_tree().quit()
	