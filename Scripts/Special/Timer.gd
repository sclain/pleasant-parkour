extends Node

func create(delay, parent, callback):
	var timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time( delay )
	timer.connect("timeout", parent, callback)
	add_child(timer)
	return timer
	
func create_loop(delay, parent, callback, autostart=true):
	var timer = Timer.new()
	timer.set_wait_time( delay )
	timer.connect("timeout", parent, callback)
	add_child(timer)
	if autostart:
		timer.start()	
	return timer
	
