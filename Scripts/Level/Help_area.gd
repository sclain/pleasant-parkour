extends Area2D

export (String) var message = ""
export (bool) var can = false
var target = null

func activate():
	G.GUI.emit_signal("display_helper_msg", message)
	
	
func _on_Key_hit(trgt):
	can = true
	activate()


func _on_Help_area_body_entered(body):
	if !can:
		return
	if body.is_in_group("player"):
		G.GUI.emit_signal("display_helper_msg", message)
