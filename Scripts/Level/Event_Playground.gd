extends Node

var storm_msg = false
var tomato_msg = false


func _on_Tempest_tempest_timeout():
	G.GUI.emit_signal("display_helper_msg", "The Tempest killed the lama !")


func _on_Level_tempest_hit():
	if !storm_msg:
		storm_msg = true
		G.GUI.emit_signal("display_helper_msg", "You can resist the storm, but be fast, the Lama can't !")


func _on_Key_hit(target):
	if !tomato_msg:
		tomato_msg = true
		G.GUI.emit_signal("display_helper_msg", "Good job ! \n Now, come back to the lama !")
