extends Node

signal tempest_timeout

onready var limit = $SCENE_LIMIT
onready var zone = $Zone
onready var L = $Zone/L
onready var R = $Zone/R

var active = true

var timer_zone

onready var time = get_parent().key_delay

var start = Vector2()
var door_distance = Vector2()
var grow = Vector2()
var grow_loss = Vector2(0,0)

onready var door_position = get_parent().get_node("Key_Door/Door").position

func _ready():
	timer_zone = G.create_timer(time, self, "stop_zone")
	start = Vector2(limit.rect_position.x, limit.rect_position.x + limit.rect_size.x)
	L.position.x = start.x-L.size.x/2
	L.start_x = L.position.x
	L.set_px_scale(Vector2(0,limit.rect_size.y))
	
	R.position.x = start.y+R.size.x/2
	R.start_x = R.position.x
	R.set_px_scale(Vector2(0,limit.rect_size.y))

	door_distance.x = abs((start.x-door_position.x))+2
	door_distance.y = abs((start.y-door_position.x))+2
	grow = door_distance/time
	start_zone()

func _process(delta):
	if !active:
		return
	zone_grow(delta)
		
func start_zone():
	timer_zone.start()
	active = true

func stop_zone():
	active = false
	emit_signal("tempest_timeout")

	
func zone_grow(delta):
	L.set_px_scale(Vector2(grow.x*delta,0))
	R.set_px_scale(Vector2(grow.y*delta,0))
	R.position.x -= grow.y*delta

		
#func zone_grow(delta):
#	var to_grow = grow*delta
#	var grow_rounded = to_grow.floor()
#	L.set_px_scale(Vector2(grow_rounded.x,0))
#	R.set_px_scale(Vector2(grow_rounded.y,0))
#	R.position.x -= grow_rounded.y
#	grow_loss += to_grow - grow_rounded
#	if grow_loss.x >=1:
#		L.set_px_scale(Vector2(1,0))
#		grow_loss.x -=1
#	if grow_loss.y >=1:
#		R.set_px_scale(Vector2(1,0))
#		R.position.x -= 1
#		grow_loss.y -=1
#
				
func reset():
	timer_zone.stop()
	L.reset_scale_x()
	R.reset_scale_x()
	grow_loss = Vector2(0,0)
	start_zone()
	
func pause():
	active = false
	timer_zone.stop()	