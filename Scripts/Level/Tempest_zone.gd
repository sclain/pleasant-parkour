extends Area2D

signal tempest_hit
var size = Vector2(64,64)
var start_x 
var grow_direction = 1
var can_hit = false
var max_scale_x = INF

func set_max_scale_x(px):
	max_scale_x = (px+size.x)/size.x
	pass
	
func set_px_scale(px):
	scale += px/size
	scale.x  = clamp(scale.x, 1, max_scale_x)
	position.y -= px.y/2
	position.x += px.x/2
	
func reset_scale_x():
	scale.x = 1
	position.x = start_x
	
func _on_Tempest_zone_body_entered(body):
	if can_hit and body.is_in_group("player"):
		if scale.x == 1:
			return 
		emit_signal("tempest_hit")
		
func _on_Tempest_zone_body_exited(body):
	pass # replace with function body
