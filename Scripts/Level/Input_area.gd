extends Area2D

export (String) var action = "Jump"
export (Array) var keys = null
export (String) var type = null
var target = null

func _on_Input_area_body_entered(body):
	if body.is_in_group("player"):
		G.GUI.emit_signal("display_helper_input", action, keys, type)
		target = body

func _on_Input_area_body_exited(body):
	if body == target:
		G.GUI.emit_signal("display_helper_input", "_reset", null, null)


func _on_Key_hit(target):
	monitoring = false
	pass # replace with function body
