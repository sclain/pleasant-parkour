extends Node

signal stop_helper_inputs
var training_msg = false

func _ready():
	G.PLAYER.connect("training", self, "_on_training_mode")
	pass


func _on_training_mode(val):	
	if val and !training_msg:
		training_msg = true
		emit_signal("stop_helper_inputs")
		G.GUI.emit_signal("display_helper_msg", "The training mode stop the storm and change your respawn position \n However you can't take the tomato \n Leave the mode when you are ready !")
