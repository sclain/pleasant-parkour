extends Area2D

const RELOAD_TIME = 5
const RESET_TIME = 1
const ATTACK_DELAY = 0.4

var loaded  = true
var attacking = false
var entered = false
var timer
var player
var sprite
var timers = {}

func _ready():
	sprite = $AnimatedSprite
	timer = $Timer
	timers["attack"] = timer.create(ATTACK_DELAY, self, "attack")
	timers["reload"] = timer.create(RELOAD_TIME, self, "reload")
	timers["reset"] = timer.create(RESET_TIME, self, "reload")
	connect("body_entered", self, "_on_TrapArea_body_entered")
	connect("body_exited", self, "_on_TrapArea_body_exited")

func _on_TrapArea_body_entered(body):
	if body.is_in_group("player"):
		entered = true
		if  !attacking and loaded:
			attacking = true
			willAttack()
		
func attack():
	sprite.play("attack_and_load")
	var collision = get_overlapping_bodies()
	for i in range(collision.size()):
		if collision[i].get_name()=="Player":
			action(collision[i])
	loaded = false
	attacking = false
	timers["reload"].start()
		
func reload():
	sprite.play("loaded")
	if entered:
		willAttack()
	loaded = true
	
func _on_TrapArea_body_exited(body):
	if entered and body.get_name()=="Player":
		entered = false
		
func willAttack():
	timers["attack"].start()
	
func action(target):
	target.ice()
	
func reset():
	loaded  = true
	attacking = false
	entered = false
	timers["reset"].start()
	
