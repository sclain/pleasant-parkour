extends Area2D

const RELOAD_TIME = 3
const ATTACK_DELAY = 0.2

var loaded  = true
var attacking = false
var entered = false
var timer
var player
var sprite
var timers = {}

func _ready():
	player = get_tree().get_root().get_child(0).get_node("Player")
	sprite = get_parent()
	timer = $Timer
	timers["attack"] = timer.create(ATTACK_DELAY, self, "attack")
	timers["reload"] = timer.create(RELOAD_TIME, self, "reload")
	connect("body_entered", self, "_on_IceTrapArea_body_entered")
	connect("body_exited", self, "_on_IceTrapArea_body_exited")

func _on_IceTrapArea_body_entered(body):
	if body == player:
		entered = true
		if  !attacking and loaded:
			attacking = true
			willAttack()
		
func attack():
	sprite.play("attack_and_load")
	var collision = get_overlapping_bodies()
	for i in range(collision.size()):
		if collision[i].get_name()=="Player":
			action()
	loaded = false
	attacking = false
	timers["reload"].start()
		
func reload():
	sprite.play("loaded")
	if entered:
		willAttack()
	loaded = true
	
func _on_IceTrapArea_body_exited(body):
	if entered and body == player:
		entered = false
		
func willAttack():
	timers["attack"].start()
	
func action():
	player.ice()


