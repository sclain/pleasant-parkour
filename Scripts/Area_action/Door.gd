extends Area2D

onready var to_look = G.PLAYER
signal win_level
var looking_left = false

func _ready():
	pass

func _process(delta):
	if looking_left:
		if to_look.position.x < position.x-64:
			looking_left = false
			scale.x*=-1
	else:
		if to_look.position.x > position.x+64:
			looking_left = true
			scale.x*=-1
			
func _on_Door_body_entered(body):
	if body.is_in_group("player"):
		if body.haveKey:	
			emit_signal("win_level")
		else:
			pass
#			$Sprite.modulate = ColorN("white",0.5)
		

func _on_Door_body_exited(body):
	if body.is_in_group("player"):
		$Sprite.modulate = ColorN("white",1)

