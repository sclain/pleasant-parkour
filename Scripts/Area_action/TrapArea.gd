extends Area2D

export (int) var RELOAD_TIME = 5
export (float) var RESET_TIME = 1
export (float) var ATTACK_DELAY = 0.325
export (int) var RELOAD_TIME_FAST= 1.5

var loaded  = true
var attacking = false
var entered = false
var timer
var sprite
onready var target = G.player
var timers = {}
var r_time

func _ready():
	sprite = $AnimatedSprite
	timer = $Timer
	timers["attack"] = timer.create(ATTACK_DELAY, self, "attack")
	timers["reload"] = timer.create(RELOAD_TIME, self, "reload")
	timers["reset"] = timer.create(RESET_TIME, self, "reload")

		
func attack():
	sprite.play("attack_and_load")
	if entered and collision_test():
		action()
	loaded = false
	attacking = false
	timers["reload"].start()
	timers["attack"].wait_time =  ATTACK_DELAY
		
func reload():
	loaded = true
	sprite.play("loaded")
	if entered:
		willAttack()
	timers["reload"].wait_time = RELOAD_TIME

	
func action():
	target.emit_signal("death")
	
func reset():
	loaded  = true
	attacking = false
	entered = false
	timers["reset"].start()
	

func collision_test():
	var b_x = round(target.position.x-target.width/2)
	var b_y = round(target.position.y-target.height/2)
	var b_width = target.width
	var b_height = target.height
	if target.crouching:
		b_y+=target.height-target.crouch_height
		b_height = target.crouch_height
	var poly = $CollisionPolygon2D.polygon
	var p1 = poly[0]
	var p2 = poly[1]
	var p3 = poly[3]
	var rot = round(rotation_degrees)	
	var a_width = abs(poly[0].x-poly[1].x)*scale.x
	var a_height = abs(poly[0].y-poly[3].y)*scale.y
	if rot == 90 or rot == -90:
		var temp = a_width
		a_width = a_height
		a_height = temp
	var a_x = round(position.x-a_width/2)
	var a_y = round(position.y-a_height/2)
	
	if b_x+b_width < a_x or b_x >a_x+a_width:
		return false
	if b_y+b_height < a_y or b_y >a_y+a_height:
		return false
	return true


func _on_Trap_body_entered(body):
	if body.is_in_group("projectile"):
		body.stop()
		timers["reload"].wait_time = RELOAD_TIME_FAST
		timers["attack"].wait_time = 0.1
		willAttack()
	if body.is_in_group("player"):
		entered = true
		willAttack()
		

func willAttack():
	if  !attacking and loaded and !target.invincible:
		attacking = true
		timers["attack"].start()
	
func _on_Trap_body_exited(body):
	if entered and body == target:
		entered = false
