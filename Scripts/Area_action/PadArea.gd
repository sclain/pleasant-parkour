extends Area2D

export (int) var POWER = -1400

func _ready():
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_PadArea_body_entered(body):
	var powr = POWER
	if body.is_in_group("player") and G.collision_body_n_area(body, self):
		if body.grapping:
			body.stop_grap()
		if body.sliding:
			if body.boost_type==3:
				powr*=1.25
		body.velocity.y = powr
