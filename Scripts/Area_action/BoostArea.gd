extends Area2D

var side
var power = 1000
var power_low = power*0.75
var force = Vector2()

func _ready():
	side = round(rotation_degrees)
	if side == 0:
		force = Vector2(0, -power)
	elif side == 90:
		force = Vector2(power_low, -power)
	elif side == 180:
		force = Vector2(0, power)
	else:
		force = Vector2(-power_low, -power)	
	pass

func _on_BoostArea_body_entered(body):
	if body.is_in_group("player"):
		if G.collision_body_n_area(body, self):
			body.impulse(force)

