extends Area2D

signal drop_item

func _on_Drop_item_area_body_entered(body):
	if body.is_in_group("player"):
		body.emit_signal("drop_items")
