extends Area2D

signal hit(target)

func _ready():
	pass
		

func _on_Key_body_entered(body):
	if body.is_in_group("player"):
			if !body._haveKey():
				emit_signal("hit", body)
				hide()
#
func reset():
	show()
