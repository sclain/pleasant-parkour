extends Node

signal items_change
signal death
signal timer_gold_timeout
signal reset
signal tempest_hit
signal timer_zone_change(time, gold)


export (bool) var Grappler = false
export (bool) var Grappler_epic = false
export (bool) var Impulse_gre = false
export (bool) var Impulse_gre_epic = false
export (int) var key_delay = 30
export (float) var key_delay_gold = 10

onready var scene_limit = $Tempest/SCENE_LIMIT
onready var timer_zone = $Tempest.timer_zone
var timer_gold
var timer_reset_delayed
var timer_loop
var items = []
var stoped = false
var picked = []
var to_reset = []
var gold = true

func _ready():
	timer_loop = G.create_loop(1, self, "on_timer_zone_request", true)
	emit_signal("timer_zone_change", key_delay, true)
	timer_gold = G.create_timer(key_delay_gold, self, "_on_timer_gold_timeout", true)
	$Tempest/SCENE_LIMIT.hide()
	$Tempest.connect("tempest_timeout", self, "_on_player_death")
	timer_reset_delayed = $Timer.create(1, self, "reset_delayed")
	init_items()
	to_reset = get_tree().get_nodes_in_group("to_reset")

					
func reset():
	if !stoped:
		timer_reset_delayed.start()
	$Key_Door/Key.reset()
	for i in range(to_reset.size()):
		to_reset[i].reset()
	drop_items()
	gold = true
	timer_loop.stop()

	
func reset_delayed():
	$Tempest.reset()
	emit_signal("timer_zone_change", key_delay, true)
	timer_loop.start()
	timer_gold.stop()
	timer_gold.start()
	$Key_Door.reset()
	emit_signal("reset")
#	$Key_Door/Key.reset_delayed()

func ending():
	timer_gold.stop()
	
func player_setup(player):
	player.createItems(items)
	
func _on_player_death():
	emit_signal("death")
	
func _on_training_set(val):
	if val:
		stoped = true
		$Tempest.pause()
	else:
		stoped = false
		reset()

			
func init_items():
	items = []
	for i in range(G.projectile.size()):
		if get(G.projectile[i]):
			items.append(G.projectile[i])
	for i in range(picked.size()):
		items.append(picked[i])
	emit_signal("items_change")
	
					
func pick_item(item):
	if !get(item):
		picked.append(item)
		items.append(item)
		return true
	return false
		
func drop_items():
	picked  = []
	init_items()
		
func _on_L_tempest_hit():
	$Key_Door.anim_tempest_hit()
	gold = false
	on_timer_zone_request()
	emit_signal("tempest_hit")

func _on_timer_gold_timeout():
	$Key_Door.anim_tempest_hit()
	gold = false
	on_timer_zone_request()
	emit_signal("timer_gold_timeout")

func _on_Level_timer_zone_change(time, type):
	pass
	
func on_timer_zone_request():
	emit_signal("timer_zone_change", timer_zone.time_left, gold)
