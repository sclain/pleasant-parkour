extends Node

signal g_loaded
signal level_change(id,gold)
onready var vp = get_tree().get_root()
var has_lvl = false

func _ready():
	G.GUI = $GUI
	G.MAIN = self
	G.PLAYER = $Player
	G.connect("set_level", self, "setLevel")
	load_save_admin()
#	load_save()
	if !setLevel(G.level_id):
		return
	level_setup()
	player_setup()
		
func setLevel(nextLevel=G.level_id+1):
	if G.level.size() < nextLevel+1 or nextLevel<0:
		if nextLevel == G.level.size():
			end_game()
		else:
			print("hummm")
		return false
		
	var add = G.level_node[nextLevel].instance()
	G.level_id = nextLevel
	if !has_lvl:
		add_child(add)
		has_lvl = true
	else:
		if $GUI.actived:
			$GUI.resume()
		call_deferred("replace_node", $Level,  add)
	return true

func player_setup():
	$Player.connect("death", self, "_on_player_death")
	$Player.connect("pick", self, "items_update")
	$Player.connect("drop_items", self, "drop_items")
	$Player.respawn()
	$Player/PlayerAction.connect("switch_item", $GUI, "_on_player_switch_item")
	$Player/PlayerAction.connect("reload_items", self, "_on_player_reload_items")

func level_setup():
	$Player.reset(false)
	$Player/Camera2D.setLimit($Level/Tempest/SCENE_LIMIT)
	$Player.connect("training", $Level, "_on_training_set")
	$Level/Key_Door/Door.connect("win_level", self, "level_win")
	$Level.connect("items_change", self, "_on_item_change")
	$Level.connect("timer_zone_change", $GUI, "on_timer_zone_change")
#	$GUI/ctrl/Player/VBoxContainer/Info/Key_Timer.update($Level/Key_Door/Key.delay)
#	$Level/Key_Door/Key.connect("key_timer_gui", $GUI/ctrl/Player/VBoxContainer/Info/Key_Timer, "update")
	$Level.connect("death", self, "_on_player_death")
	items_send()

func _on_item_change():
	items_send()
	
func items_send():
	$Level.player_setup($Player)
	var items = $Player/Inventory.items
	$GUI/ctrl/Player.add_items(items)

func items_update(item):
	if $Level.pick_item(item):
		items_send()

func drop_items():
	$Level.drop_items()
		
func _on_player_reload_items(array):
	$GUI._on_player_reload_items(array)
			
func level_win():
	$Level.ending()
	var gold = level_perf_setup()
	emit_signal("level_change", G.level_id, gold)
	setLevel()
	if G.level_id>G.level_best:
		G.level_best = G.level_id

func level_perf_setup():
	var time = $Level.key_delay-$Level.timer_zone.time_left
	var gold = $Level.gold
	print("time: ",time)
	return G.set_record(time, G.level_id, gold).gold
		
	
func _process(delta):
	var debug_nextLevel = Input.is_action_just_pressed('nextLevel')
	var debug_previousLevel = Input.is_action_just_pressed('previous_level')
	if debug_nextLevel:
		level_win()
	if debug_previousLevel:
		setLevel(G.level_id-1)

func replace_node(suppr, add):
	$Player.invincible = true
	suppr.queue_free()
	remove_child(suppr)
	add_child(add)
	level_setup()
	$Player.respawn()
	
func end_game():
	$GUI.ending_screen.show()
	
func _on_player_death():
	$Player.die()
	$Level.reset()
	
	
func load_save_admin():
	G.level_best = 99
	G.loaded = true
	emit_signal("g_loaded")

	
func load_save():
	G.game_save = true
	var save_game = File.new()
	if not save_game.file_exists("user://save_game.json"):
		pass
	else:
		var err = save_game.open_encrypted_with_pass("user://save_game.json", File.READ, "gotaga")
		if err == 0:
			var data = save_game.get_as_text()
			var save = parse_json(data)
			G.level_best = save["level_best"]
			G.level_id = save["level_id"]
			for i in range(save["level"].size()):
				if G.level_map.has(save["level"][i].name):
					G.level[G.level_map[save["level"][i].name]] = save["level"][i]
			if G.level_best<G.level_id:
				G.level_best = G.level_id
	if not save_game.file_exists("user://save_settings.json"):
		pass
	else:
		save_game.open("user://save_settings.json", File.READ)
		var data = save_game.get_as_text()
		var save = parse_json(data)
		for i in save:
			G.set(i, save[i])
	emit_signal("g_loaded")
	G.loaded = true

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		G.leave_game()
		


	